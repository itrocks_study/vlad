<html>

<?php
$link = mysqli_connect('localhost', 'root', '0229', 'forum');
if (!$link) {
    die('Ошибка соединения: ' . mysqli_error());
}

$sql = "CREATE DATABASE forum;";
if (mysqli_query($link, $sql)) {
    echo "База forum успешно создана\n";
} else {
    echo 'Ошибка при создании базы данных: ' . mysqli_error() . "\n";
}

$user = 'CREATE TABLE Users (
 user_id int unsigned auto_increment,
 email varchar(30),
 fio varchar(30),
 phone varchar(12),
 last_access datetime,
 PRIMARY KEY (user_id)
 );';

if (mysqli_query($link, $user)) {
    echo "Таблица user успешно создана\n";
} else {
    echo 'Ошибка при создании таблицы данных: ' . mysqli_error() . "\n";
}

$blog = 'CREATE TABLE Blog (
 blog_id int unsigned auto_increment,
 user_id int unsigned not null,
 image varchar(50), 
 title varchar(30),
 data datetime,
 short_d varchar(60),
 detailed_d varchar(200),
 PRIMARY KEY (blog_id),
 FOREIGN KEY (user_id) references Users (user_id)
 );';

if (mysqli_query($link, $blog)) {
    echo "Таблица blog успешно создана\n";
} else {
    echo 'Ошибка при создании таблицы данных: ' . mysqli_error() . "\n";
}

?>



</html>
